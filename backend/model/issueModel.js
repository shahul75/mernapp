const mongoose = require('mongoose')
const issueSchema = mongoose.Schema({
    issueText :{
        type : String,
        required : [true, 'Please add issue text']
    }
    },{
        timestamps: true
    }
    
)

module.exports = mongoose.model('Issue' , issueSchema)