const express = require('express')
const { get } = require('mongoose')
const bodyParser = require('body-parser')
const router = express.Router()
const  {getIssue, setIssue, updateIssue, deleteIssue} = require('../controllers/issueController')

router.route('/').get(getIssue).post(setIssue)

router.route('/:id').put(updateIssue).delete(deleteIssue)

module.exports = router