const asynchHandler = require('express-async-handler')
const Issue = require('../model/issueModel')

// @desc get Issues
// @route GET /api/issues
// @access Private
const getIssue = asynchHandler(async(req, res) => {
    console.log(req.body.text)
    const issues = await Issue.find()
    res.status(200).json(issues)
})

// @desc get Issues
// @route POST /api/issues
// @access Private
const setIssue = asynchHandler(async(req, res) => {
   console.log(req.body.text)
    if (!req.body.text) {
        res.status(400)
        throw new Error("Incorrect Payload")
    }

    const issue = await Issue.create({
        issue : req.body.text
    })

    res.status(200).json(issue)
})

// @desc get Issues
// @route PUT /api/issues/:id
// @access Private
const updateIssue = asynchHandler(async(req, res) => {
    res.status(200).json({message : `Issue updated ${req.params.id}`})
})

// @desc get Issues
// @route DELETE /api/issues/:id
// @access Private
const deleteIssue = asynchHandler(async(req, res) => {
    res.status(200).json({message : `Issue Deleted ${req.params.id}`})
})

module.exports = {
    getIssue, setIssue, updateIssue, deleteIssue
}