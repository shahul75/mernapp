const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const colors = require('colors')
const dotenv = require('dotenv').config()
const {errorHandler} = require('./middleware/errorMiddleware')
const connectDB = require('./config/db')
const routes = require('./routes/issueRoutes')
connectDB()
const port = process.env.PORT

const app = express()
//app.use(express.json())

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended : true}))

app.use('/api/issues', routes)

//app.use(errorHandler)

app.listen(port, () => console.log(`Server Started on port ${port}`))